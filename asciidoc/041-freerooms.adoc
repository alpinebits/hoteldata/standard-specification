[[anchor-4.1]]
=== 4.1. FreeRooms: room availability notifications

When the value of the [request_param]`action` parameter is `OTA_HotelInvCountNotif:FreeRooms` the client intends to send room availability notifications to the server.

A server that supports this action *must* support at least one of two capabilities: `OTA_HotelInvCountNotif_accept_rooms` or `OTA_HotelInvCountNotif_accept_categories`.
This way the server indicates whether it can handle the availability of rooms at the level of distinct rooms, at the level of categories of rooms or both (a better explanation will follow).


[[anchor-4.1.1]]
==== 4.1.1. Client request

The parameter [request_param]`request` must contain an `OTA_HotelInvCountNotifRQ` document.

Consider the outer part of the example document:

|===
a[freerooms_bkg]|
[source,xml]
----
include::includes/4.1.1-FreeRooms-OTA_HotelInvCountNotifRQ-outer.txt[]
----
v| [small]**`samples/FreeRooms/FreeRooms-OTA_HotelInvCountNotifRQ.xml`** - outer part
|===
An `OTA_HotelInvCountNotifRQ` contains exactly *one* [xml_element_name]#Inventories# (note the plural) element, hence at most *one* hotel can be dealt with in a single request.

{AlpineBitsR} requires the *mandatory* attribute [xml_attribute_name]#HotelCode# to be present and match information in the server's database.
The attribute [xml_attribute_name]#HotelName# is *optional*.

Second, consider the inner part of the example that contains [xml_element_name]#Inventory# elements (note the singular) for three different time periods.

|===
a[freerooms_bkg]|
[source,xml]
----
include::includes/4.1.1-FreeRooms-OTA_HotelInvCountNotifRQ-inner.txt[]
----
v|[small]**`samples/FreeRooms/FreeRooms-OTA_HotelInvCountNotifRQ.xml`** - inner part
|===
The absence of the [xml_attribute_name]#InvCode# attribute tells us we're dealing with a room category (DOUBLE) given by the [xml_attribute_name]#InvTypeCode#.

Alternatively, using a [xml_attribute_name]#InvCode# together with a [xml_attribute_name]#InvTypeCode# attribute would indicate that the availability refers to a *specific room*, not a room category.

{AlpineBitsR} *requires* a match of [xml_attribute_name]#InvCode# or [xml_attribute_name]#InvTypeCode# to be *case sensitive*.

An {AlpineBitsR} server that implements the FreeRooms action *must* be able to treat *at least* one out of two cases (specific rooms or categories).
A client should perform the `OTA_Ping:Handshaking` action to find out whether the server treats the room case (token `OTA_HotelInvCountNotif_accept_rooms`), the category case (token `OTA_HotelInvCountNotif_accept_categories`) or both.

Mixing rooms and categories in a single request is *not* allowed.
An {AlpineBitsR} server *must* return an error if it receives such a mixed request.

The [xml_element_name]#Inventory# element for a given room/room category can contain an [xml_element_name]#InvCounts# (plural) element.
Based on the supported capabilities, the [xml_element_name]#InvCounts# element contains from 1 to 3 [xml_element_name]#InvCount# (singular) elements, one for each of the allowed [xml_attribute_name]#CountType# attributes:

* [xml_attribute_name]#CountType# = `2` indicates rooms that are *bookable* (this [xml_attribute_name]#CountType# *must* be supported).

* [xml_attribute_name]#CountType# = `6` indicates rooms that are *out of order* (i.e. rooms that are not booked nor bookable, maybe due to renovations), requires the `OTA_HotelInvCountNotif_accept_out_of_order` capability.

* [xml_attribute_name]#CountType# = `9` indicates rooms that are *available but not bookable* (see section <<anchor-4.2,GuestRequests>> for the description of bookings), requires the `OTA_HotelInvCountNotif_accept_out_of_market` capability.

If the server does not have the corresponding capability set, a client *must not* send the [xml_attribute_name]#CountType# = `6` or the [xml_attribute_name]#CountType# = `9` elements, and all the rooms not specified in the [xml_attribute_name]#CountType# = `2` element are to be considered as already booked.

The integer value of the attribute [xml_attribute_name]#Count# indicates the room count for the given [xml_attribute_name]#CountType# and must be a non negative number (≥ 0).

If an [xml_element_name]#InvCount# element for a specific [xml_attribute_name]#CountType# is missing it must be considered as if its [xml_attribute_name]#Count# attribute is 0. Likewise, if the whole [xml_element_name]#InvCounts# container is missing it is considered as if the whole room/room category is fully booked in the given time period (same as [xml_attribute_name]#CountType# = `2` with [xml_attribute_name]#Count# = `0`).

[xml_attribute_name]#Count# numbers are always interpreted to be absolute numbers. Differential updates are not allowed.

Since in the example we’re dealing with a room category ([xml_attribute_name]#InvCode# is missing), the value of [xml_attribute_name]#Count# can be > 1.
In the case of a specific room ([xml_attribute_name]#InvCode# is specified), the only meaningful value would be 1 (the same room can not be "counted" more than once).

Since overbooking is not allowed and the number of counted rooms (bookable or not) cannot exceed the total number of rooms, the inequality 0 ≤ 'sum of [xml_attribute_name]#Count# values' ≤ 'total number of rooms' holds.

Considering the example, the attributes [xml_attribute_name]#Start# and [xml_attribute_name]#End# and the corresponding [xml_element_name]#InvCount# element indicate that room cateogry DOUBLE has 3 rooms available from 2020-08-01 to 2020-08-10 and 1 room available from 2020-08-21 to 2020-08-30; from 2020-08-11 to 2020-08-20 there are no DOUBLE rooms available.

Regarding the first interval, this means the earliest possible check-in is 2020-08-01 afternoon and latest possible check-out is 2020-08-11 morning (maximum stay is 10 nights).

Check-ins *after* 2020-08-01 and stays of *less* than 10 nights are allowed as well, provided the check-out is not later than the morning of 2020-08-11.

Same applies for the third block of 10 nights from 2020-08-21 to 2020-08-30 (latest check-out is 2020-08-31 morning).

During the second time period, from 2020-08-11 to 2020-08-20, no check-in is possible.

Note that {AlpineBitsR} does *not allow* [xml_element_name]#Inventory# elements with overlapping periods regarding the same room or room category.
This implies that the order of the [xml_element_name]#Inventory# elements doesn't matter.
It is a *client's responsibility* to avoid overlapping.
An {AlpineBitsR} server's business logic *may* identify overlapping and return an error or *may* proceed in an implementation-specific way.

===== CompleteSet

Clients and servers typically wish to exchange only delta information about room availabilities in order to keep the total amount of data to be processed in check.

However, {AlpineBitsR} considered several use-cases in which it is important to transmit the complete availability information, as might be the case for a first synchronization or for aligning two systems after a communication issue.
For this reason it defines a request that has the purpose of resetting the server information with that contained in the message. Such request is called a `CompleteSet`.

If a server provides the capability `OTA_HotelInvCountNotif_accept_complete_set`, then a client *may* send a `CompleteSet` message as explained below.

In the example, the [xml_element_name]#UniqueID# element with attribute [xml_attribute_name]#Instance# = `CompleteSet` and [xml_attribute_name]#Type# = `16` indicates that this message contains the complete information (as entered by the user).
When receiving such a request, a server *must* remove all information about any availability it might have on record regarding the given hotel.

The attribute [xml_attribute_name]#ID# is needed for compatibility with OTA, the value is ignored by {AlpineBitsR}.

In a `CompleteSet` message *all* the rooms/room categories managed by the client must be specified for any time period for which the client has data on records.
This means that also in cases where a room/room category is fully booked (with no availability), it must have a corresponding [xml_element_name]#Inventory# element with its own [xml_element_name]#StatusApplicationControl# sub element and a coherent set of [xml_element_name]#InvCount# elements contained in it (or no [xml_element_name]#InvCounts# container at all, as stated above).
This states unambiguously that the room/room category has no availability for the specified period.

A client *must not* include in a `CompleteSet` message a period for which it has no data on record (for instance a client that is forwarding data received from a source, *must not* forward any information besides what was originally received from the source).

Normally the FreeRooms message requires exactly one [xml_element_name]#StatusApplicationControl# element with attributes [xml_attribute_name]#Start#, [xml_attribute_name]#End#, [xml_attribute_name]#InvTypeCode# (and optional [xml_attribute_name]#InvCode#) for each [xml_element_name]#Inventory# element.
The server *must* return an error if any of these are missing.
There is however one exception: to completely reset all room availability information for a given hotel a client might send a `CompleteSet` request with just one empty [xml_element_name]#Inventory# element without any attributes.
The presence of the empty [xml_element_name]#Inventory# element is required for OTA validation.

In addition, a client might want to let a server know its availability data, should be purged based on internal business rules.
An example might be availability data that is considered stale, because it hasn’t been updated by the user for some time.
The client *should* then send a request using an [xml_element_name]#UniqueID# element with attribute [xml_attribute_name]#Instance# = `CompleteSet` and [xml_attribute_name]#Type# = `35`.
A server *must* accept this value (without returning an error or warning) and *could* make use of this hint and keep the availability data it has on record flagging it as purged.
Otherwise such a message should be considered equivalent to the one with [xml_attribute_name]#Type# = `16`.

===== Deltas

If the [xml_element_name]#UniqueID# element is missing, the message contains *delta information*. In that case the server updates only the information that is contained in the message without touching the other information that it has on record.

A server that supports delta requests *must* indicate so via the `OTA_HotelInvCountNotif_accept_deltas` capability. As always, it is the *client's responsibility* to check whether the server supports deltas before trying to send them.

{AlpineBitsR} *recommends* that implementers that use delta requests *should* send the full set of information periodically if both client and server support it.

If the entire period is explicitly transmitted and processsed in a delta message, this leads to the complete overwriting of the previous situation.

If a server does not support `CompleteSet` messages, it might lead to cases where the hotel would have to intervene and delete obsolete data from the server manually in its backend.

A server *should* provide at least one of `OTA_HotelInvCountNotif_accept_deltas` and `OTA_HotelInvCountNotif_accept_complete_set` capabilities in order to accept FreeRooms messages.

===== Closing seasons

A special [xml_element_name]#Inventory# element can be used to send information about closing seasons, i.e. time periods in which the hotel is completely closed.
It is in fact important to distinguish the cases in which a hotel is fully booked (where one could still hope for a last minute cancellation) from those in which a hotel is closed (where it could not even answer the phone).

Closing seasons elements require both parties to expose the `OTA_HotelInvCountNotif_accept_closing_seasons` capability during handshaking and can *only* be sent as first [xml_element_name]#Inventory# elements in a `CompleteSet` message.
The content of the closing seasons [xml_element_name]#Inventory# element is defined as follows:

* *One* [xml_element_name]#StatusApplicationControl# sub element indicating a time period with the *mandatory* [xml_attribute_name]#Start# and [xml_attribute_name]#End# attributes and with the mandatory attribute [xml_attribute_name]#AllInvCode# set to `true`.

* *No* [xml_element_name]#InvCounts# sub elements are allowed.

Multiple [xml_element_name]#Inventory# elements can be specified this way to indicate multiple closing periods.

Each of the closing time periods *must not* overlap with other closed periods nor with any time period that states a room/room category is available (it is therefore possible for a closed period to overlap with a period where all the rooms/room categories are *unavailable*). A server *must* return an error if it detects such inconsistencies.

Delta messages are still to be considered as the most up to date, so in case a delta message would set a room/room category as available in a period previously set as closed, it must be considered as if the hotel revoked the closed period for the overlapping days.

As a best practice, the client should avoid such overwriting or at least send a complete set afterwards in order to explicitly set the correct closed periods.
If the server deems it appropriate to force a full synchronization, it might request a `CompleteSet` in one of its next responses (see <<anchor-2.3,section 2.3>>).


[[anchor-4.1.2]]
==== 4.1.2. Server response

The server will send a response indicating the outcome of the request.
The response is a `OTA_HotelInvCountNotifRS` document.
Any of the four possible {AlpineBitsR} server response outcomes (success, advisory, warning or error)
are allowed.

See <<anchor-2.3,section 2.3>> for details.


[[anchor-4.1.3]]
==== 4.1.3. Implementation tips and best practice

* Note that in the 2011-11 version of {AlpineBitsR} the support of this action was mandatory for the server. This is no longer the case.

* Note that sending partial information (deltas) was added with {AlpineBitsR} 2013-04.

* Note that this action has been completely re-written in the 2020-10 version of {AlpineBitsR}.

* It is important that applications that forward data received from other sources (e.g. channel managers and alike) do not forward any information not present in the original data. For example, if such an application manages on its system a wider time frame than the one received from the source, it should not forward any information beyond the most future date it has received.
This is necessary to keep the information clean through the forwarding without complementary (and arbitrary) data to be added by third party applications.

* For `CompleteSet` requests, since no time frame is explicitly transmitted by the client, a server is encouraged to delete and insert all information stored in its backend, rather than updating it.

* Please note that the [xml_attribute_name]#End# date of an interval identifies the last day and night of the stay. Departure is the morning of the day *after* the [xml_attribute_name]#End# date.

* Please note that previous versions of {AlpineBitsR} allowed some booking restrictions to be used in FreeRooms (length of stay and day of arrival). This possibility has been removed with version 2014-04 as these restrictions are better handled by RatePlans.

[[anchor-4.1.4]]
==== 4.1.4. Tabular representation of OTA_HotelInvCountNotifRQ
include::tables/OTA_HotelInvCountNotifRQ.adoc[]
link:file:///files/schema-xsd/alpinebits.xsd[AlpineBits XSD Schema]

[[anchor-4.1.5]]
==== 4.1.5. Tabular representation of OTA_HotelInvCountNotifRS
include::tables/OTA_HotelInvCountNotifRS.adoc[]
link:file:///files/schema-xsd/alpinebits.xsd[AlpineBits XSD Schema]
