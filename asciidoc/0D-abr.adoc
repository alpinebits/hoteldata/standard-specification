[[anchor-D.ABR]]
=== ABR: AlpineBits Room Amenity Type

[cols="1,4",options="header"]
|===
|Value |Description
|1.ABR |2 or more bathrooms
|2.ABR |2 or more bedrooms
|3.ABR |Accessible rooms
|4.ABR |Additional bed
|5.ABR |Air conditioning
|6.ABR |Baby cot
|7.ABR |Balcony
|8.ABR |Barrier-free
|9.ABR |Bath towels
|10.ABR |Bathrobe
|11.ABR |Bathroom amenities
|12.ABR |Bathtub
|13.ABR |Bed linen
|14.ABR |Bidet
|15.ABR |Bunk bed
|16.ABR |Cleaning upon request
|17.ABR |Coffee machine
|18.ABR |Crockery
|19.ABR |Daily cleaning included
|20.ABR |Dependance
|21.ABR |Desk
|22.ABR |Dishwasher
|23.ABR |Dormitory
|24.ABR |Double bed
|25.ABR |Double sofa bed
|26.ABR |Eat-in Kitchen
|27.ABR |Electric stove
|28.ABR |Electrical adaptors available
|29.ABR |Final cleaning included
|30.ABR |Free Wifi
|31.ABR |Fully equipped eat-in kitchen
|32.ABR |Garden
|33.ABR |Gas connection
|34.ABR |Gas stove
|35.ABR |Hairdryer
|36.ABR |Hi-Fi system
|37.ABR |Hob
|38.ABR |Hotel annex
|39.ABR |Hypoallergenic room
|40.ABR |Induction hob
|41.ABR |Infrared cabin
|42.ABR |Internet access
|43.ABR |Iron
|44.ABR |Ironing board
|45.ABR |Kettle
|46.ABR |Lake view
|47.ABR |Main building
|48.ABR |Microwave
|49.ABR |Minibar
|50.ABR |Mountain view
|51.ABR |Non-smoking
|52.ABR |Oven
|53.ABR |Panorama view
|54.ABR |Pets
|55.ABR |Pets on request
|56.ABR |Refrigerator
|57.ABR |Room For Allergy-sufferers
|58.ABR |Room with connecting door
|59.ABR |Safe
|60.ABR |Satellite/cable TV
|61.ABR |Sauna
|62.ABR |Sea view
|63.ABR |Seasonal rent possible
|64.ABR |Separate kitchen
|65.ABR |Separate living area
|66.ABR |Separate WC
|67.ABR |Shower On The Floor
|68.ABR |Shower/WC
|69.ABR |Single bed
|70.ABR |Single room
|71.ABR |Single sofa bed
|72.ABR |Snack bar
|73.ABR |South Side
|74.ABR |Stove
|75.ABR |Suite
|76.ABR |Tea/Coffee machine
|77.ABR |Telephone
|78.ABR |Terrace
|79.ABR |Tiled stove/firplace
|80.ABR |Towels
|81.ABR |TV
|82.ABR |TV upon request
|83.ABR |Twin room
|84.ABR |Washing machine
|85.ABR |Water boiler
|86.ABR |Water/waste connection
|87.ABR |WC on the floor
|88.ABR |Whirlpool
|89.ABR |With hotel service
|===