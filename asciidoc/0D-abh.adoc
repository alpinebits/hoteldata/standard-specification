[[anchor-D.ABH]]
=== ABH: AlpineBits Hotel Amenity Code

[cols="1,4",options="header"]
|===
|Value |Description
|1.ABH |Agreement with golf course
|2.ABH |Agreement with indoor pool
|3.ABH |Agreement with outdoor pool
|4.ABH |Animation
|5.ABH |Baby change
|6.ABH |Babysitter
|7.ABH |Bank card/Maestro
|8.ABH |Bar
|9.ABH |Barbecue area
|10.ABH |Barbecue facilities
|11.ABH |Barbeque area
|12.ABH |Barrier-free accomodation
|13.ABH |Beauty farm
|14.ABH |Bicycle rental
|15.ABH |Bike depot
|16.ABH |Bread delivery service
|17.ABH |Bread roll delivery service
|18.ABH |Breakfast buffet
|19.ABH |Breakfast on request
|20.ABH |Central location
|21.ABH |Changing table
|22.ABH |Chargeable Wi-Fi in room / apt.
|23.ABH |Charging station for electric vehicle
|24.ABH |Child care
|25.ABH |Choice of menus possible
|26.ABH |Close to a bus stop (< 100 m)
|27.ABH |Close to public transport
|28.ABH |Coach tours
|29.ABH |Conf./event organisation possible
|30.ABH |Conference and event facilities
|31.ABH |Continental breakfast
|32.ABH |Continental breakfast/Brunch
|33.ABH |Credit card
|34.ABH |Cuisine without glutine
|35.ABH |Cycling
|36.ABH |Dietary cuisine
|37.ABH |Directly at the cross-country track
|38.ABH |Directly at the lake
|39.ABH |Directly at the lift
|40.ABH |Directly on the ski slope
|41.ABH |Directly on the slope
|42.ABH |Doctor or medical care
|43.ABH |Dogs allowed
|44.ABH |Dogs and pets on request
|45.ABH |Drying room
|46.ABH |E-bike rental
|47.ABH |Elevator
|48.ABH |Entertainment evenings
|49.ABH |Families
|50.ABH |Fitness room
|51.ABH |Free access to lake/private beach
|52.ABH |Free WiFi in room / apt.
|53.ABH |Fresh bread service
|54.ABH |Fruit growing farm
|55.ABH |Garden
|56.ABH |Gluten-free dishes
|57.ABH |Golf
|58.ABH |Guided cycling tours
|59.ABH |Guided ski tours
|60.ABH |Guided tours and hikes
|61.ABH |Hairdresser
|62.ABH |Handicapped accessible
|63.ABH |Hiking equipment
|64.ABH |Hiking trail
|65.ABH |Horse riding
|66.ABH |Hot showers
|67.ABH |Hotel information channel
|68.ABH |Hotel skipass service
|69.ABH |Indoor pool
|70.ABH |Infrared cabin
|71.ABH |In-house info channel
|72.ABH |International cuisine
|73.ABH |Internet access in room/apartment
|74.ABH |Jogging trail
|75.ABH |Lactose-free dishes
|76.ABH |Lactose-free food
|77.ABH |Laptop on request
|78.ABH |Laptop upon request
|79.ABH |Large pets
|80.ABH |Laundry/Laundry service
|81.ABH |Library
|82.ABH |Livestock farm
|83.ABH |Lounge
|84.ABH |Luggage transfer
|85.ABH |Massages
|86.ABH |Mediterranean cuisine
|87.ABH |Midsize pets
|88.ABH |Miniature golf
|89.ABH |Motorbikes welcome
|90.ABH |Motorcycle depot
|91.ABH |Mountain biking/ bike trail
|92.ABH |Mountain climbing
|93.ABH |MTB rental
|94.ABH |Natural, local remedies
|95.ABH |Near a skiing and hiking area
|96.ABH |Near bus stop
|97.ABH |Near ski bus stop
|98.ABH |No pets or domestic animals
|99.ABH |Open car park
|100.ABH |Outdoor pool
|101.ABH |Own horse riding stable
|102.ABH |Panoramic location
|103.ABH |Pick-up service
|104.ABH |Ping Pong
|105.ABH |Pizzeria
|106.ABH |Playground
|107.ABH |Playroom
|108.ABH |Polo
|109.ABH |Public bar
|110.ABH |Quiet location
|111.ABH |Racing bike rental
|112.ABH |Reception 24 h
|113.ABH |Relaxation area
|114.ABH |Residence bar
|115.ABH |Restaurant
|116.ABH |River rafting
|117.ABH |Rock climbing
|118.ABH |Roofed car park
|119.ABH |Sales of home-made products
|120.ABH |Salt grotto
|121.ABH |Sanitary cabins for rent
|122.ABH |Sauna
|123.ABH |Self service laundry
|124.ABH |Seminar room
|125.ABH |Seniors
|126.ABH |Shared kitchen
|127.ABH |Shop / Shopping possibility
|128.ABH |Shop/shopping facility
|129.ABH |Shopping facility 200m away
|130.ABH |Shuttle service
|131.ABH |Sightseeing tours
|132.ABH |Ski bus / Ski shuttle
|133.ABH |Ski depot
|134.ABH |Ski rental
|135.ABH |Ski service
|136.ABH |Sledge rental
|137.ABH |Small pets allowed
|138.ABH |Smoking room
|139.ABH |Snack bar
|140.ABH |Snacks / small in-between meals
|141.ABH |Snacks/Small dishes in-between
|142.ABH |Snacks/tidbits between meals
|143.ABH |Snowboard
|144.ABH |Snow-shoe rental
|145.ABH |Solarium
|146.ABH |Spa licence (state license/agreement)
|147.ABH |Spa treatments
|148.ABH |Special agreement golf course
|149.ABH |Special agreement indoor pool
|150.ABH |Special agreement outdoor pool
|151.ABH |Sports animation
|152.ABH |Steam bath
|153.ABH |Stroller rental
|154.ABH |Supermarket at < 200m
|155.ABH |Surf point
|156.ABH |Tennis court
|157.ABH |Terrace
|158.ABH |Thermal baths
|159.ABH |Toilet cubicles
|160.ABH |Tumble drier
|161.ABH |Underground car park
|162.ABH |Vegan cuisine
|163.ABH |Vegetarian cuisine
|164.ABH |Visa
|165.ABH |Volleyball
|166.ABH |Walking routes
|167.ABH |Washing machine
|168.ABH |Welcome drink
|169.ABH |Wellness / Spa
|170.ABH |Whirlpool
|171.ABH |WiFi
|172.ABH |Windsurf
|173.ABH |Wine cellar
|174.ABH |Wine growing farm
|175.ABH |Wine tasting
|===