<MultimediaDescriptions>

  <MultimediaDescription InfoCode="25">
    <TextItems>
      <TextItem>
        <Description TextFormat="PlainText" Language="en">
          Double room</Description>
        <Description TextFormat="PlainText" Language="de">
          Doppelzimmer</Description>
        <Description TextFormat="PlainText" Language="it">
          Camera doppia</Description>
      </TextItem>
    </TextItems>
  </MultimediaDescription>

  <MultimediaDescription InfoCode="1">
    <TextItems>
      <TextItem>
        <Description TextFormat="PlainText" Language="en">
          Description of the double room.</Description>
        <Description TextFormat="PlainText" Language="de">
           Doppelzimmer Beschreibung.</Description>
        <Description TextFormat="PlainText" Language="it">
           Descrizione della camera doppia.</Description>
      </TextItem>
    </TextItems>
  </MultimediaDescription>

  <MultimediaDescription InfoCode="23">
    <ImageItems>
      <!-- 6 means guest room, see OTA table PIC -->
      <ImageItem Category="6">
        <ImageFormat CopyrightNotice="Copyright notice 2015">
          <URL>http://www.example.com/image.jpg</URL>
        </ImageFormat>
        <Description TextFormat="PlainText" Language="en">
          Picture of the room</Description>
        <Description TextFormat="PlainText" Language="de">
          Zimmerbild</Description>
        <Description TextFormat="PlainText" Language="it">
          Immagine della stanza</Description>
      </ImageItem>
    </ImageItems>
  </MultimediaDescription>

<MultimediaDescriptions>
