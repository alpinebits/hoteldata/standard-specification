<HotelInfo>
	<!-- type of propriety like: Hotel, B&B, Chalet... -->
	<CategoryCodes>
		<HotelCategory CodeDetail="ASTAT2020_11:4s"/> 
	</CategoryCodes>
	
	<Descriptions>
		<MultimediaDescriptions>
			<!-- InfoCode="1" for long description -->
			<MultimediaDescription InfoCode="1">
				<TextItems>
					<TextItem  CopyrightNotice="Other copyright">
						<Description Language="en" TextFormat="PlainText">A long description</Description>
					</TextItem> 
					<TextItem  CopyrightNotice="Other copyright">
						<Description Language="de" TextFormat="PlainText">erweiterte Beschreibung</Description>
					</TextItem>
					<TextItem  CopyrightNotice="Other copyright">
						<Description Language="it" TextFormat="PlainText">Descrizione lunga</Description>
					</TextItem>
				</TextItems>
			</MultimediaDescription>
			
			<!-- hotel pictures -->
			<MultimediaDescription InfoCode="23">
				<ImageItems>
					<!-- example: exterior picture applicable from Sep 30 to Mar 30 for seasonal pictures -->
					<ImageItem Category="1">
						<ImageFormat CopyrightNotice="Image copyright" SourceID="56757e0211440d37910f407fb6f657fb" 
									 ApplicableStart="--09-30" ApplicableEnd="--03-30">
							<URL>https://..../HotelExteriorWinterView.jpg</URL>
						</ImageFormat>
						<Description TextFormat="PlainText" Language="en">Image description</Description>
					</ImageItem>
				</ImageItems>
			</MultimediaDescription>
		</MultimediaDescriptions>
	</Descriptions>
	
	<!-- geo position -->
	<Position Altitude="200" AltitudeUnitOfMeasureCode="3" Latitude="46.1372647" Longitude="12.2011353"/>
	
	<!-- hotel amenities -->
	<Services>
		<!-- hotel level facilities see (HAC) codelist -->
		<Service Code="68"  ProximityCode="2"/>
		<Service Code="47"  ProximityCode="3">
			<Features>
				<!-- See PHY Disability Feature Code-->
				<Feature AccessibleCode="8"/>
			</Features>
		</Service>
	</Services>
</HotelInfo>
