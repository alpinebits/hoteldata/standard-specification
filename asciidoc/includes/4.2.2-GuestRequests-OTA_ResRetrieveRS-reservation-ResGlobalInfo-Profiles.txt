<Profiles>
    <ProfileInfo>
        <!-- ProfileType 4 -> Travel Agent -->
        <Profile ProfileType="4">
            <CompanyInfo>
                <CompanyName Code="123" CodeContext="ABC">
                    ACME Travel Agency
                </CompanyName>
                <AddressInfo>
                    <AddressLine>Musterstraße 1</AddressLine>
                    <CityName>Flaneid</CityName>
                    <PostalCode>12345</PostalCode>
                    <CountryName Code="IT"/>
                </AddressInfo>
                <!-- Code 1 -> Voice -->
                <TelephoneInfo PhoneTechType="1" PhoneNumber="+391234567890"/>
                <Email>info@example.com</Email>
            </CompanyInfo>
        </Profile>
    </ProfileInfo>
</Profiles>