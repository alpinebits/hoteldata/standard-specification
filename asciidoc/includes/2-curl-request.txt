POST /server-2024-10/ HTTP/1.1
Host: development.alpinebits.org
Authorization: Basic Y2hyaXM6c2VjcmV0
User-Agent: curl/7.64.0
Accept: */*
X-AlpineBits-ClientProtocolVersion: 2024-10
Content-Length: 1472
Content-Type: multipart/form-data; boundary=------------------------cd287e7912d06e8f
Expect: 100-continue

--------------------------cd287e7912d06e8f
Content-Disposition: form-data; name="action"
 
OTA_Ping:Handshaking
--------------------------cd287e7912d06e8f
Content-Disposition: form-data; name="request"
Content-Type: application/xml
 
[here is the content of the file Handshake-OTA_PingRQ.xml]
--------------------------cd287e7912d06e8f--
