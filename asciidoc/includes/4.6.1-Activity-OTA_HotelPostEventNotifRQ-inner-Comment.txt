<Comments>
    <Comment Name="Title">
        <Text Language="en">Yoga with Cesare</Text>
        <Text Language="de">Yoga mit Cesare</Text>
        <Text Language="it">Yoga con Cesare</Text>
    </Comment>
    <Comment Name="Category">
        <Text>WELLNESS-MENTAL</Text>
        <Text Language="en">Mental wellness</Text>
        <Text Language="de">mentales Wohlbefinden</Text>
        <Text Language="it">Benessere mentale</Text>
    </Comment>
    <Comment Name="Gallery">
        <Image>https://example.com/image1.jpg</Image>
        <Image>https://example.com/image2.jpg</Image>
    </Comment>
    <Comment Name="Description">
        <Text Language="en">Yoga Lorem Ipsum ...</Text>
        <Text Language="de">Yoga Lorem Ipsum ...</Text>
        <Text Language="it">Yoga Lorem Ipsum ...</Text>
    </Comment>
</Comments>
