curl --trace-ascii -  
     -H "X-AlpineBits-ClientProtocolVersion: 2024-10" 
     --user user:secret 
     -F "action=OTA_Ping:Handshaking" 
     -F "request=<Handshake-OTA_PingRQ.xml"
     http://development.alpinebits.org/server-2024-10/
